package com.example.kjankiewicz.android_11w02_mytestopenglesapplication

import android.opengl.GLES20
import android.util.Log

import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer

internal class Cube {

    private val vertexBuffer: FloatBuffer
    private val colorsBuffer: FloatBuffer

    private val vertexShaderCode = "uniform mat4 uMVPMatrix; " +
            "attribute vec4 vPosition; " +
            "attribute vec4 vColor; " +
            "varying vec4 color; " +
            "void main() {" +
            "  gl_Position = uMVPMatrix * vPosition; " +
            "  color = vColor; " + // vPosition

            "}"

    private val fragmentShaderCode = "precision mediump float;" +
            " varying vec4 color;" +
            "void main() {" +
            "  gl_FragColor = color; " +
            "}"

    private val vertexCount = cubeCoords.size / COORDS_PER_VERTEX
    private val vertexStride = COORDS_PER_VERTEX * 4

    private val mProgram: Int

    private var mPositionHandle: Int = 0
    private var mColorHandle: Int = 0

    private var mMVPMatrixHandle: Int = 0

    init {
        val bbv = ByteBuffer.
                allocateDirect(cubeCoords.size * 4)
        bbv.order(ByteOrder.nativeOrder())
        vertexBuffer = bbv.asFloatBuffer()
        vertexBuffer.put(cubeCoords)
        vertexBuffer.position(0)

        val bbc = ByteBuffer.
                allocateDirect(cubeColors.size * 4)
        bbc.order(ByteOrder.nativeOrder())
        colorsBuffer = bbc.asFloatBuffer()
        colorsBuffer.put(cubeColors)
        colorsBuffer.position(0)

        val vertexShader: Int = loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode)
        val fragmentShader: Int = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode)

        mProgram = GLES20.glCreateProgram()
        GLES20.glAttachShader(mProgram, vertexShader)
        GLES20.glAttachShader(mProgram, fragmentShader)
        GLES20.glLinkProgram(mProgram)
    }

    fun draw(mvpMatrix: FloatArray) {

        GLES20.glUseProgram(mProgram)

        mPositionHandle = GLES20.glGetAttribLocation(mProgram,
                "vPosition")
        GLES20.glEnableVertexAttribArray(mPositionHandle)
        GLES20.glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer)

        mColorHandle = GLES20.glGetAttribLocation(mProgram,
                "vColor")
        GLES20.glEnableVertexAttribArray(mColorHandle)
        GLES20.glVertexAttribPointer(mColorHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, colorsBuffer)

        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram,
                "uMVPMatrix")
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1,
                false, mvpMatrix, 0)

        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount)

        GLES20.glDisableVertexAttribArray(mPositionHandle)
        GLES20.glDisableVertexAttribArray(mColorHandle)
    }

    companion object {

        // number of coordinates per vertex in this array
        private const val COORDS_PER_VERTEX = 4

        private val cubeCoords = floatArrayOf(1.0f, -1.0f, -1.0f, 2.0f, -1.0f, 1.0f, -1.0f, 2.0f, -1.0f, -1.0f, -1.0f, 2.0f,
                1.0f, -1.0f, -1.0f, 2.0f, 1.0f, 1.0f, -1.0f, 2.0f, -1.0f, 1.0f, -1.0f, 2.0f,
                -1.0f, -1.0f, 1.0f, 2.0f, 1.0f, 1.0f, 1.0f, 2.0f, 1.0f, -1.0f, 1.0f, 2.0f,
                -1.0f, -1.0f, 1.0f, 2.0f, -1.0f, 1.0f, 1.0f, 2.0f, 1.0f, 1.0f, 1.0f, 2.0f,
                1.0f, -1.0f, 1.0f, 2.0f, 1.0f, 1.0f, -1.0f, 2.0f, 1.0f, -1.0f, -1.0f, 2.0f,
                1.0f, -1.0f, 1.0f, 2.0f, 1.0f, 1.0f, 1.0f, 2.0f, 1.0f, 1.0f, -1.0f, 2.0f,
                -1.0f, -1.0f, -1.0f, 2.0f, -1.0f, 1.0f, 1.0f, 2.0f, -1.0f, -1.0f, 1.0f, 2.0f,
                -1.0f, -1.0f, -1.0f, 2.0f, -1.0f, 1.0f, -1.0f, 2.0f, -1.0f, 1.0f, 1.0f, 2.0f,
                -1.0f, -1.0f, -1.0f, 2.0f, 1.0f, -1.0f, 1.0f, 2.0f, 1.0f, -1.0f, -1.0f, 2.0f,
                -1.0f, -1.0f, -1.0f, 2.0f, -1.0f, -1.0f, 1.0f, 2.0f, 1.0f, -1.0f, 1.0f, 2.0f,
                -1.0f, 1.0f, 1.0f, 2.0f, 1.0f, 1.0f, -1.0f, 2.0f, 1.0f, 1.0f, 1.0f, 2.0f,
                -1.0f, 1.0f, 1.0f, 2.0f, -1.0f, 1.0f, -1.0f, 2.0f, 1.0f, 1.0f, -1.0f, 2.0f)
        private val cubeColors = floatArrayOf(1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
                1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
                0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f,
                0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f,
                0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
                0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
                1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
                1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
                0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f,
                0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f,
                1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
                1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f)

        private fun loadShader(type: Int, shaderCode: String): Int {

            val shader = GLES20.glCreateShader(type)

            GLES20.glShaderSource(shader, shaderCode)
            GLES20.glCompileShader(shader)

            val msg = GLES20.glGetShaderInfoLog(shader)
            Log.d("Cube",msg)

            return shader
        }
    }

}
