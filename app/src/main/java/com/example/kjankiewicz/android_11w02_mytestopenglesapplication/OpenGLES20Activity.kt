package com.example.kjankiewicz.android_11w02_mytestopenglesapplication

import android.app.Activity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast


class OpenGLES20Activity : Activity() {

    private lateinit var mGLView: MyGLSurfaceView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Create a GLSurfaceView instance and set it
        // as the ContentView for this Activity.
        mGLView = MyGLSurfaceView(this)
        setContentView(mGLView)
    }

    var triangleDrawing = true
    var cubeDrawing = false
    var teapotDrawing = false

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.open_gles20, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            R.id.triangle_drawing_checkbox -> {
                item.isChecked = !item.isChecked
                triangleDrawing = item.isChecked
                mGLView.refreshContent()
                return true
            }

            R.id.cube_drawing_checkbox -> {
                item.isChecked = !item.isChecked
                cubeDrawing = item.isChecked
                mGLView.refreshContent()
                return true
            }

            R.id.teapot_drawing_checkbox -> {
                item.isChecked = !item.isChecked
                teapotDrawing = item.isChecked
                mGLView.refreshContent()
                return true
            }

            R.id.opengles_30_availability_item -> Toast.makeText(applicationContext, mGLView.openGLExtensions, Toast.LENGTH_LONG).show()
        }
        return super.onOptionsItemSelected(item)
    }
}
