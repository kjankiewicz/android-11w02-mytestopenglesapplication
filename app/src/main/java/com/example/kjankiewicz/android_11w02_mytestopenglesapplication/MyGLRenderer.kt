package com.example.kjankiewicz.android_11w02_mytestopenglesapplication

import android.content.Context
import android.opengl.GLES20
import android.opengl.GLSurfaceView
import android.opengl.Matrix
import android.util.Log


import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

class MyGLRenderer
    internal constructor(private val mContext: Context) :
        GLSurfaceView.Renderer {

    private lateinit var mTeapot: Teapot
    private lateinit var mTriangle: Triangle
    private lateinit var mCube: Cube

    @Volatile
    internal var xAngle: Float = 0.toFloat()
    @Volatile
    internal var yAngle: Float = 0.toFloat()

    private val mMVPMatrix = FloatArray(16)
    private val mProjectionMatrix = FloatArray(16)
    private val mRotationXMatrix = FloatArray(16)
    private val mRotationYMatrix = FloatArray(16)
    private val mViewMatrix = FloatArray(16)

    private val mModelMatrix = FloatArray(16)
    private val mMVPMatrixAfterXRotation = FloatArray(16)
    private val mMVPMatrixAfterXYRotation = FloatArray(16)

    internal lateinit var openGLExtensions: String

    private fun determineGraphicSupport() {
        openGLExtensions = GLES20.glGetString(GLES20.GL_EXTENSIONS)
        val renderer = GLES20.glGetString(GLES20.GL_RENDERER)
        val isSoftwareRenderer = renderer.
                contains("PixelFlinger")
        val supportsDrawTexture = openGLExtensions.
                contains("draw_texture")
        val arGlMaxTextureSize = IntArray(1)
        GLES20.glGetIntegerv(GLES20.GL_MAX_TEXTURE_SIZE,
                arGlMaxTextureSize, 0)

        Log.i("MyGLRenderer", openGLExtensions)
        Log.i("MyGLRenderer", "Graphics using: "
                + "  OpenGL renderer: " + renderer
                + ". Software renderer: " + isSoftwareRenderer
                + ". Support draw texture: " + supportsDrawTexture
                + ". Texture max size: " + arGlMaxTextureSize[0])
    }

    override fun onSurfaceCreated(unused: GL10, config: EGLConfig) {
        determineGraphicSupport()
        // Set the background frame color
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f)
        // Enable depth analysis
        GLES20.glEnable(GLES20.GL_DEPTH_TEST)
        mTriangle = Triangle()
        mCube = Cube()
        mTeapot = Teapot(mContext)
    }

    override fun onDrawFrame(unused: GL10) {
        // Redraw background color
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT or GLES20.GL_DEPTH_BUFFER_BIT)

        // Set the camera position (View matrix)
        Matrix.setLookAtM(mViewMatrix, 0, 0f, 0f, -5f,
                0f, 0f, 0f, 0f, 1.0f, 0.0f)

        // Calculate the projection and view transformation
        Matrix.multiplyMM(mMVPMatrix, 0,
                mProjectionMatrix, 0, mViewMatrix, 0)

        // Create a rotation transformation
        Matrix.setRotateM(mRotationXMatrix, 0, xAngle, 0f, 1f, 0f)
        Matrix.setRotateM(mRotationYMatrix, 0, yAngle, 1f, 0f, 0f)

        Matrix.multiplyMM(mMVPMatrixAfterXRotation, 0,
                mMVPMatrix, 0, mRotationXMatrix, 0)
        Matrix.multiplyMM(mMVPMatrixAfterXYRotation, 0,
                mMVPMatrixAfterXRotation, 0, mRotationYMatrix, 0)

        if ((mContext as OpenGLES20Activity).triangleDrawing)
            mTriangle.draw(mMVPMatrixAfterXYRotation)

        if (mContext.cubeDrawing)
            mCube.draw(mMVPMatrixAfterXYRotation)

        // Create ModelMatrix for teapot shaders
        if (mContext.teapotDrawing) {
            Matrix.multiplyMM(mModelMatrix, 0, mRotationXMatrix,
                    0, mRotationYMatrix, 0)
            mTeapot.draw(mProjectionMatrix, mViewMatrix, mModelMatrix)
        }
    }

    override fun onSurfaceChanged(unused: GL10, width: Int, height: Int) {
        GLES20.glViewport(0, 0, width, height)
        val ratio = width.toFloat() / height
        Matrix.perspectiveM(mProjectionMatrix, 0, 30f,
                ratio, 1f, 20f)
    }

}
