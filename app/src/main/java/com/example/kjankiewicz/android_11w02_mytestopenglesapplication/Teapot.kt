package com.example.kjankiewicz.android_11w02_mytestopenglesapplication

import android.content.Context
import android.opengl.GLES20
import android.util.Log

import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer

/**
 * By Witold Andrzejewski exercises
 */
internal class Teapot(private val mContext: Context) {
    private val vertexBuffer: FloatBuffer
    private val normalBuffer: FloatBuffer

    private val mProgram: Int

    private var mPositionHandle: Int = 0
    private var mNormalHandle: Int = 0
    private var mColorHandle: Int = 0

    private val vertexShaderCode = "precision mediump float;" +
            "uniform mat4 P; " + //uMVPMatrix

            "uniform mat4 V; " +
            "uniform mat4 M; " +
            "varying vec4 iN;" +
            "varying vec4 iL;" +
            "varying vec4 iV;" +
            "attribute vec4 vPosition; " +
            "attribute vec4 vNormal; " +
            "uniform vec4 lightPos;" +
            "void main() {" +
            " gl_Position = P*V*M * vPosition;" +
            " iN=normalize(V*M*vNormal); " +
            " iL=normalize(V*lightPos-V*M*vPosition); " +
            " iV=normalize(vec4(0,0,0,1)-V*M*vPosition); " +
            "}"

    private val fragmentShaderCode = "precision mediump float;" +
            "uniform vec4 vColor;" +
            "varying vec4 iN;" +
            "varying vec4 iL;" +
            "varying vec4 iV;" +
            "void main() {" +
            " vec4 mN=normalize(iN);" +
            " vec4 mV=normalize(iV);" +
            " vec4 mL=normalize(iL);" +
            " vec4 mR=reflect(-mL,mN);" +
            " float a=dot(mN,mL);" +
            " if (a<0.0) a=0.0;" +
            " float b=dot(mR,mV);" +
            " if (b<0.0) b=0.0;" +
            " b=pow(b,50.0);" +
            " gl_FragColor = vec4(vColor.rgb*a,1) + vec4(b,b,b,0);" +
            "}"

    private val teapotCoords: FloatArray
    private val teapotNormals: FloatArray
    private var vertexCount: Int = 0

    // Set color with red, green, blue and alpha (opacity) values
    private val color = floatArrayOf(0.63671875f, 0.76953125f, 0.22265625f, 1.0f)

    init {
        teapotCoords = getFloatArrayFromResource(R.array.teapotVerticesArray, 2976 * 4)
        vertexCount = teapotCoords.size / COORDS_PER_VERTEX

        val bb = ByteBuffer.allocateDirect(teapotCoords.size * 4)
        bb.order(ByteOrder.nativeOrder())
        vertexBuffer = bb.asFloatBuffer()
        vertexBuffer.put(teapotCoords)
        vertexBuffer.position(0)

        teapotNormals = getFloatArrayFromResource(R.array.teapotNormals2Array, 2976 * 4)
        vertexCount = teapotNormals.size / COORDS_PER_VERTEX
        val bbn = ByteBuffer.allocateDirect(teapotNormals.size * 4)
        bbn.order(ByteOrder.nativeOrder())
        normalBuffer = bbn.asFloatBuffer()
        normalBuffer.put(teapotNormals)
        normalBuffer.position(0)

        val vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode)
        val fragmentShader = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode)

        mProgram = GLES20.glCreateProgram()             // create empty OpenGL ES Program
        GLES20.glAttachShader(mProgram, vertexShader)   // add the vertex shader to program
        GLES20.glAttachShader(mProgram, fragmentShader) // add the fragment shader to program
        GLES20.glLinkProgram(mProgram)                  // creates OpenGL ES program executables

        // Check if the linking errors exists
        val linkStatus = IntArray(1)
        GLES20.glGetProgramiv(mProgram, GLES20.GL_LINK_STATUS, linkStatus, 0)
        if (linkStatus[0] != GLES20.GL_TRUE) {
            Log.e(TAG, "Could not link program: ")
            Log.e(TAG, GLES20.glGetProgramInfoLog(mProgram))
            GLES20.glDeleteProgram(mProgram)
        }
    }

    private fun getFloatArrayFromResource(arrayID: Int, limit: Int): FloatArray {
        val ta = mContext.resources.obtainTypedArray(arrayID)
        //ta.recycle()
        val floatArray = FloatArray(limit)
        for (i in 0 until limit)
            floatArray[i] = ta.getFloat(i, 0f)
        return floatArray
    }

    fun draw(pMatrix: FloatArray, vMatrix: FloatArray, mMatrix: FloatArray) {

        GLES20.glUseProgram(mProgram)

        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition")
        mNormalHandle = GLES20.glGetAttribLocation(mProgram, "vNormal")

        GLES20.glEnableVertexAttribArray(mPositionHandle)
        GLES20.glEnableVertexAttribArray(mNormalHandle)

        val vertexStride = COORDS_PER_VERTEX * 4
        GLES20.glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer)
        GLES20.glVertexAttribPointer(mNormalHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, normalBuffer)

        mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor")

        GLES20.glUniform4fv(mColorHandle, 1, color, 0)

        val mLightPosHandle = GLES20.glGetUniformLocation(mProgram, "lightPos")
        GLES20.glUniform4f(mLightPosHandle, 5f, 5f, -5f, 1f)


        val mPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "P")
        val mVMatrixHandle = GLES20.glGetUniformLocation(mProgram, "V")
        val mMMatrixHandle = GLES20.glGetUniformLocation(mProgram, "M")

        GLES20.glUniformMatrix4fv(mPMatrixHandle, 1, false, pMatrix, 0)
        GLES20.glUniformMatrix4fv(mVMatrixHandle, 1, false, vMatrix, 0)
        GLES20.glUniformMatrix4fv(mMMatrixHandle, 1, false, mMatrix, 0)

        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount)

        GLES20.glDisableVertexAttribArray(mPositionHandle)
    }

    companion object {

        // number of coordinates per vertex in this array
        private const val COORDS_PER_VERTEX = 4
        //float color[] = { 0.686274509f, 0.474509803f, 0.19607843f, 1.0f }; light bronze

        private const val TAG = "Teapot"

        private fun loadShader(type: Int, shaderCode: String): Int {

            val shader = GLES20.glCreateShader(type)


            GLES20.glShaderSource(shader, shaderCode)
            GLES20.glCompileShader(shader)
            // Check if the compilation errors exists
            val msg = GLES20.glGetShaderInfoLog(shader)
            Log.d(TAG, msg)

            return shader
        }
    }
}
